import './index.scss';
import React from 'react';
import { IonContent, IonPage, IonCardContent, IonCard, IonCardTitle, IonCardSubtitle } from '@ionic/react';
import { LoggedInHeader } from '../../components';
import { useHistory } from 'react-router';
import { Box, Typography } from '@mui/material';
import notransactionhistory from './../../assets/img/notransactionhistory.png';

export const TransactionHistory = () => {

    const history = useHistory();

    var data = [
        {
            id: 1,
            name: 'aysuh dubey',
            title: "your credit balance",
            amount: "+10000",
            status: 0,
        },
        {
            id: 2,
            name: "niraj mishra",
            title: "your credit balance",
            amount: "+10000",
            status: 0,
        },
        {
            id: 4,
            name: 'rishabh mishra',
            title: "your credit balance",
            amount: "+10000",
            status: 0,
        },
        {
            id: 5,
            name: 'shinu tech',
            title: "your debit balance",
            amount: -100,
            status: 1,
        },
        {
            id: 6,
            name: 'umesh yadav',
            title: "your debit balance",
            amount: "-50000",
            status: 1,
        },
        {
            id: 7,
            name: 'deepak singh',
            title: "your debit balance",
            amount: -500,
            status: 1,
        }
    ]



    return (
        <IonPage >
            <IonContent>
                <LoggedInHeader />
                <Box className="ion-padding centerimg">
                    {/* <img src={notransactionhistory} /> */}
                </Box>
                {/* <div className="nogames">
                    <Typography variant="h5">No transactions yet!</Typography>
                    <Typography>Seems like you haven’t done any activity yet</Typography>
                </div> */}



                {/* <IonCardHeader>
                            <IonCardTitle>Card Title</IonCardTitle>
                        </IonCardHeader> */}
                {
                    data.map((item) => (
                        <>
                            <IonCard>
                                <IonCardContent className='mt-2 pt-2' style={{
                                    color: 'black'
                                }}>
                                    <IonCardTitle>{item.name}</IonCardTitle>
                                    <div>{item.title}</div>
                                    {
                                        item.status == 1 ? (<>
                                            <IonCardSubtitle style={{
                                                float: 'right',
                                                color:'red',
                                                marginTop:'-3%'
                                            }}>
                                                {
                                                    item.amount
                                                }
                                            </IonCardSubtitle>
                                        </>) : (<>
                                            <IonCardSubtitle style={{
                                                float: 'right',
                                                color:'green',
                                                marginTop:'-3%'
                                            }}>
                                                {
                                                    item.amount
                                                }
                                            </IonCardSubtitle>
                                        </>)
                                    }



                                </IonCardContent>



                            </IonCard>
                        </>

                    ))
                }



            </IonContent>
        </IonPage>
    )
}