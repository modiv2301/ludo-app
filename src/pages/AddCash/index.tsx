import classes from './index.module.scss';
import React, { useState, useCallback, useEffect } from 'react'
import { IonButton, IonContent, IonFooter, IonItem, IonLabel, IonPage } from '@ionic/react';
import { AmountCard, LoggedInHeader } from '../../components';
import { Box, Stack, TextField, Typography } from '@mui/material';
import { FormattedMessage } from 'react-intl';
import { AllPay, GPay, NetBanking, UPI } from '../../Icons';
import { Razorpay } from '../../Icons/Razorpay';
import { IonAvatar } from '@ionic/react';
import razorpay from '../../assets/img/Razorpay-logo.png';
import useRazorpay from "react-razorpay";
import axios from 'axios'
import { postwallet } from '../../apis';
import { useHistory } from 'react-router';


// interface RpayProps {
//     style?: React.CSSProperties;
// };


export const AddCash = () => {
    const history = useHistory();

    const [profileDetail, setProfileDetail] = React.useState<any>({
        "username": "",
        "userid": "",
        "useremail": "modiv2301@gmail.com",
    });
    useEffect(()=>{
        setProfileDetail({...profileDetail, "username":localStorage.getItem('username')})
        setProfileDetail({...profileDetail, "userid":localStorage.getItem('userid')})
    },[])
    console.log(localStorage.getItem('username'),"detaillllllllllllll");
    const [amount, setAmount] = React.useState<string>("");
    const [step, setStep] = React.useState<number>(1);

    let id = localStorage.getItem("userid")

    // console.log(JSON.stringify(id))

    const Razorpay = useRazorpay();

    // const displayRazorpay = useCallback((amount) => {

    //     // console.log("my amount" + amount)

    //     var changeamount = amount*100;

    //     const options = {
    //         key: "rzp_test_GYl71wgx7cqEWs",
    //         amount: changeamount.toString(),
    //         currency: "INR",
    //         name: "Ludo Project",
    //         description: "Your Transaction",
    //         image: razorpay,
    //         order_id: "",
    //         handler: (response: any) =>{
    //             const data ={
    //                 orderCreationId: 1,
    //                 razorpayPaymentId: response.razorpay_payment_id,
    //                 razorpayOrderId: response.razorpay_order_id,
    //                 razorpaySignature: response.razorpay_signature,
    //             }
    //             const result = axios.post('https://thecompletesoftech.com/ludo/public/api/v1/customer/wallet', data)
    //             alert(result);

    //         },
    //         prefill: {
    //             name: 'vivek modi',
    //             email: 'example@example.com',
    //             contact: '9999999999',
    //         },
    //         notes: {
    //             address: 'Example Corporate Office',
    //         },
    //         theme: {
    //             color: '#61dafb',
    //         },
    //     };

    //     const paymentObject = new Razorpay(options);

    //     paymentObject.on("payment.failed", function (response: any) {
    //         alert(response.error.code);
    //         alert(response.error.description);
    //         alert(response.error.source);
    //         alert(response.error.step);
    //         alert(response.error.reason);
    //         alert(response.error.metadata.order_id);
    //         alert(response.error.metadata.payment_id);
    //     });

    //     paymentObject.open();

    // }, [Razorpay]);


    const displayRazorpay = async (amount: any,id:any) => {
        

        let changeAmount = amount * 100
        const options = {
            key: "rzp_test_GYl71wgx7cqEWs", 
            amount: changeAmount.toString(), 
            currency: "INR",
            name: "Acme Corp",
            description: "Test Transaction",
            image: razorpay,
            order_id:"", 
            handler: async function (response: any) {
                
                console.log("my res", JSON.stringify(response))
                 
                const data = {
                    user_id: profileDetail.userid,
                    wallet_amount: amount,
                    status:"1",
                    transaction_id: response.razorpay_payment_id,
                }

                console.log("data =>"+JSON.stringify(data))

                const result = await axios.post('http://localhost:8000/api/v1/customer/wallet', data)
                history.push('/battle')

            },
            prefill: {
                name: profileDetail.username,
                email: "modiv2301@gmail.com",
                contact: "9999999999",
            },
            notes: {
                address: "Razorpay Corporate Office",
            },
            theme: {
                color: "#3399cc",
            },
        };
        
        console.log(options)

        const rzp1 = new Razorpay(options);

        // rzp1.on("payment.failed", function (response: any) {
            // alert(response.error.code);
            // alert(response.error.description);
            // alert(response.error.source);
            // alert(response.error.step);
            // alert(response.error.reason);
            // alert(response.error.metadata.order_id);
            // alert(response.error.metadata.payment_id);
        // });

        rzp1.open();
    };

    return (
        <IonPage>
            <IonContent fullscreen>
                <LoggedInHeader />
                {step === 1 && <div className="ion-padding" style={{ paddingTop: '5%' }}>
                    <Typography variant="h5" sx={{ fontWeight: "bold", color: 'gray' }}>
                        <FormattedMessage id="add_cash.declaration" />
                    </Typography>
                    <Stack spacing={3}>
                        <div className="ion-padding" style={{ paddingTop: '5rem' }}>
                            <TextField
                                label="Amount"
                                type="number"
                                id="outlined-size-small"
                                size="medium"
                                fullWidth
                                color="primary"
                                value={amount}
                                onChange={(_e) => setAmount(_e.target.value)}
                            />
                        </div>
                        <Box sx={{
                            display: 'flex',
                            flexWrap: 'wrap',
                            justifyContent: 'space-around'
                        }}>
                            <AmountCard amount={10} onClick={amount => setAmount(String(amount))} />
                            <AmountCard amount={100} onClick={amount => setAmount(String(amount))} />
                            <AmountCard amount={200} onClick={amount => setAmount(String(amount))} />
                            <AmountCard amount={300} onClick={amount => setAmount(String(amount))} />
                            <AmountCard amount={400} onClick={amount => setAmount(String(amount))} />
                        </Box>
                    </Stack>
                </div>}

                {step === 2 && <div className="ion-padding" style={{ paddingTop: '5%', background: '#ededed' }}>
                    <Box sx={{
                        display: 'flex',
                        flexWrap: 'wrap',
                        justifyContent: 'space-between',
                        padding: '.5rem',
                        border: '2px solid #d2d2d2',
                        paddingInline: '0.5rem'
                    }}>
                        <Typography variant="h5" sx={{ fontWeight: "bold", color: 'gray', paddingTop: '.5rem' }}>
                            <FormattedMessage id="add_cash.added" /> <span className="color-primary">{amount}</span>
                        </Typography>
                        <IonButton shape='round' fill="outline" className={classes.edit} onClick={() => setStep(1)}>
                            <FormattedMessage id="add_cash.edit_button" />
                        </IonButton>
                    </Box>

                    <div style={{ paddingTop: '3rem' }}>
                        <Typography style={{ textAlign: 'left', padding: '0', fontWeight: 'bold', color: 'gray' }}>Pay Via</Typography>
                        <Stack spacing={3}>
                            {/* <GPay />
                            <UPI />
                            <NetBanking />
                            <AllPay /> */}
                            {/* <Razorpay /> */}
                            <IonItem style={{ border: '4px solid lightgray', borderRadius: "15px" }}>
                                <IonAvatar >
                                    <img src={razorpay} />
                                </IonAvatar>
                                <IonLabel style={{ fontWeight: 'bold', paddingLeft: '10px', color: 'gray' }} onClick={() => displayRazorpay(amount,id)}>Razorpay</IonLabel>
                            </IonItem>
                        </Stack>
                    </div>
                </div>}

            </IonContent>
            {step === 1 && <IonFooter>
                <IonButton expand="full" shape="round" size="large" style={{ margin: "2rem" }} onClick={() => setStep(2)} disabled={!amount}>NEXT </IonButton>
            </IonFooter>}
        </IonPage>
    )
}
