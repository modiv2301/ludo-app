import classes from './index.module.scss';

import React, { useEffect, useState } from 'react';

import { IonContent, IonPage } from '@ionic/react';

import { LoggedInHeader } from '../../components';

import { useHistory } from 'react-router';

import { Box, ImageList, ImageListItem, Typography } from '@mui/material';

import { FormattedMessage } from 'react-intl';

// import d1 from './../../assets/img/d1.png';
// import d2 from './../../assets/img/d2.jpg';
// import d3 from './../../assets/img/d3.jpg';
// import d4 from './../../assets/img/d4.jpg';

import { CgLivePhoto } from 'react-icons/cg';

import { allgame } from '../../apis';

// const diceMap = [d4, d2, d3, d4];

interface DashboardProps { };

export const Dashboard = (props: DashboardProps) => {

    const history = useHistory();

    const [game,setgame] = useState<any[]>([])

    const storetoken = localStorage.getItem("usertoken")


    const getallgame = async () => {
      
        try {
         
            let response = await allgame();
            // console.log("my data is" + JSON.stringify(response.data.data));
            setgame(response.data.data)
          
        } catch (error) {
            console.error("Getting error at auth: ", error);
         
        }
    };

    useEffect(() => {
        if (storetoken == null) {
            history.push('/login')
        }
        else {
            history.push('/dashboard')
        }
        getallgame()
    }, [])



    return (
        <IonPage>

            <IonContent fullscreen>

                <LoggedInHeader />

                <Typography className={classes.head}>
                    <FormattedMessage id="dashboard.our_games" />
                </Typography>

                <Box className={classes.ourGames} onClick={() => history.push('/battle')} />

                <Typography className={classes.head}>
                    <FormattedMessage id="dashboard.trending_games" />
                </Typography>

                <ImageList>
                    {
                        game.map((item,index) =>(
                            <ImageListItem className={classes.imageGrid} key={"dice" + index}>
                                     <label className={classes.live}><CgLivePhoto style={{ verticalAlign: "middle",paddingBottom:'3%'}}/> Live</label>
                                    <img src={"https://thecompletesoftech.com/ludo/public/files/game/" + item.banner_image} style={{
                                        borderRadius:'15px'
                                    }}/>
                                    </ImageListItem>
                        ))
                    }
                    {/* {game.map((d, index) => <ImageListItem className={classes.imageGrid} key={"dice" + index}>
                        <label className={classes.live}><CgLivePhoto style={{ verticalAlign: "middle" }} /> Live</label>
                        <img
                            src={d.banner_image}
                            srcSet={d}
                            loading="lazy"
                            style={{ borderRadius: '25px', objectFit: 'revert' }}
                            alt=""
                        />
                    </ImageListItem>
                    )} */}
                </ImageList>
            </IonContent>
        </IonPage>
    )
}