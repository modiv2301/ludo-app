import './index.scss';
import React, { useEffect, useState } from 'react';
import {
    IonContent, IonPage, IonTitle, IonToolbar,
    IonHeader, IonLabel, IonIcon, IonItem, IonCardContent, IonCardTitle, IonCardSubtitle, IonCardHeader, IonCard
} from '@ionic/react';
import { LoggedInHeader } from '../../components';
import { useHistory } from 'react-router';
import { Box, Typography } from '@mui/material';
import nonotification from './../../assets/img/nonotification.png';

export const Notification = () => {

    // const [data, setdata] = useState([]);

    const [isloading, setisloading] = useState(false);


    var data = [
        {
            id: 1,
            title: "My first blog post"
        },
        {
            id: 2,
            title: "My second blog post",
        },
        {
            id: 3,
            title: "My first blog post"
        },
        {
            id: 4,
            title: "My second blog post",
        },
        {
            id: 5,
            title: "My first blog post"
        },
        {
            id: 6,
            title: "My second blog post",
        }
    ]


    const getnoti = () => {
        setisloading(true)
    }


    useEffect(() => {
        getnoti()
    }, [])

    const history = useHistory();


    return (
        <IonPage>
            <IonContent fullscreen>
                <LoggedInHeader />
                <Box className="ion-padding centerimg">
                    {/* <img src={nonotification} /> */}

                    <IonCard>
                        {/* <IonCardHeader>
                            <IonCardTitle>Card Title</IonCardTitle>
                        </IonCardHeader> */}
                        {
                            data.map((item) => (
                                <>

                                    <IonCardContent className='mt-2 pt-2' style={{
                                        width: '90rem',
                                        color: 'black'
                                    }}>
                                        {item.title}

                                    </IonCardContent>
                                </>

                            ))
                        }

                    </IonCard>
                </Box>
            </IonContent>
        </IonPage>
    )
}