import classes from './index.module.scss';
import React, { useEffect, useState } from 'react';
import { IonContent, IonPage } from '@ionic/react';
import { LoggedInHeader, WalletCard } from '../../components';
import { Stack } from '@mui/material';
import axios from 'axios'

interface WalletProps { };

export const Wallet = (props: WalletProps) => {
    const[walletHistory, setWalletHistory] = useState([]);
    useEffect(()=>{
        let data = {
            "user_id" : localStorage.getItem('userid')
        }
        axios.post('http://localhost:8000/api/v1/customer/wallet_history', data)
        .then((response)=> {
            console.log(response.data.data);
            setWalletHistory(response.data.data);
        }) 
        .catch((error)=>{
            console.log(error);
        })
    },[])
    return (
        <IonPage>
            <IonContent fullscreen>
                <LoggedInHeader />
                {/* <div style={{ paddingTop: '2rem' }}>
                    <WalletCard
                        buttonTitle={"Deposit Cash"}
                        subheading={"Deposit Cash"}
                        amount={85.5} description="This can be used for deposit."
                    />
                    <WalletCard
                        buttonTitle={"Withdraw Cash"}
                        subheading={"Withdraw Cash"}
                        amount={80.5} description="This can be used for withdraw."
                    />
                </div> */}
                {walletHistory.length ? walletHistory.map((transaction)=>(
                   <WalletCard
                        buttonTitle={"Deposit Cash"}
                        subheading={"Deposit Cash"}
                        amount={transaction.wallet_amount} description="This can be used for deposit."
                    /> 
                )) : ""}
            </IonContent>
        </IonPage>
    )
}
