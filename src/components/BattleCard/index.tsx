import classes from './index.module.scss';
import { IonAvatar, IonIcon } from '@ionic/react';
import { Box, Typography } from '@mui/material';
import React from 'react';
import { CgProfile } from 'react-icons/cg';
import money from './../../assets/img/money.svg';
import male from './../../assets/img/male.png';
import female from './../../assets/img/female.png';
import battle from './../../assets/img/energy.png';
import { IonButton, IonCheckbox, IonContent, IonFooter, IonItem, IonLabel, IonList, IonPage } from '@ionic/react';

interface BattleCardProps {
    amount?: number;
    onClick?: Function;
    image?: string;
    battleId?: number;
};

export const BattleCard = (props: BattleCardProps) => {
    return (
        <Box onClick={(_e: any) => props.onClick ? props.onClick(_e) : undefined} sx={{
            borderRadius: '15px',
            margin: '.5rem',
            backgroundColor: '#f7f7f7',
        }}>
            <Typography variant="subtitle1" sx={{ fontWeight: 'bold', color: 'gray', paddingLeft: '1rem' }}>
                Playing for <IonIcon style={{ width: "3rem", height: "3rem", verticalAlign: "middle" }} src={money} /> <span style={{ fontSize: 'large', color: 'darkred' }}>{props.amount}</span>
            </Typography>
            <Box className={classes.battle} sx={{
                display: 'flex',
            }}>
                <div style={{ width: '100%', textAlign: 'center' }}>
                    <div >
                        <img src={props.image} />
                    </div>
                </div>
                
            </Box>
        </Box>
    )
}
